package concurrent;

import java.util.concurrent.Executors;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.ForkJoinTask;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import static java.util.concurrent.TimeUnit.*;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;

public class LockTypes {
	private final ScheduledExecutorService scheduler =
		     Executors.newScheduledThreadPool(1);

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		LockTypes testor=new LockTypes();
		//lt.scheduledThreadPool();
		//testor.fixedSizeThreadPool();
		testor.forkJoinPool();
		}

	
	public void scheduledThreadPool(){
		
		final Runnable beeper = new Runnable() {
		       public void run() { System.out.println("beep"); }
		     };
		     final ScheduledFuture<?> beeperHandle =
		       scheduler.scheduleAtFixedRate(beeper, 10, 10, SECONDS);
		     scheduler.schedule(new Runnable() {
		       public void run() { beeperHandle.cancel(true); }
		     }, 60 * 60, SECONDS);
		
	}
	
	public void fixedSizeThreadPool(){
		ExecutorService pool=Executors.newFixedThreadPool(10);
		List<Callable<Long>> tasks=new ArrayList<Callable<Long>>();
		
		for (int i=0; i <100; i++){
			Callable t=new Caller();
			tasks.add(t);
		}
		
		try {
			
			List<Future<Long>> tids=pool.invokeAll(tasks);
			for (Future<Long> future:tids){
				Long tid=future.get();
				System.out.println("Thread id is : " + tid);
			}
			pool.shutdown();
			
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public void forkJoinPool() {
		ForkJoinPool pool=new ForkJoinPool(4);
		for (int i=0; i< 100; i++){
			//Callable<Long> task=new Caller();
			Runnable task=new Runner();
			pool.execute(task);
			//ForkJoinTask<Long> t=pool.submit(task,1200l);
			try {
				Thread.sleep(10);
				//System.out.println(t.get());
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			//Long tid=pool.invoke(t);
			//System.out.println("Thread Id is \t" + tid);
		}
		
		
	}
}
