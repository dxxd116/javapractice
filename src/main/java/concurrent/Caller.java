package concurrent;

import java.util.concurrent.Callable;

public class Caller implements Callable<Long>{
	
	private long t1=System.currentTimeMillis() ;

		
	public Long call(){
		long tid=Thread.currentThread().getId();
		System.out.println("Thread Id:\t" + tid + "\t\tThread time:\t" + t1);
		return tid;
	}
}
