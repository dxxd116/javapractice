package java8;

import java.time.Duration;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Set;
import java.time.temporal.ChronoUnit;

public class TimeSample {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		TimeSample testor=new TimeSample();
		//testor.timer();
		//testor.calculateDate();
		//testor.timestamp();
		testor.timeMath();
	}
	
	
	public void timer(){
		Set<String> zones=ZoneId.getAvailableZoneIds();
		zones.forEach(x -> System.out.println(x));
		LocalDate now=LocalDate.now(ZoneId.of("America/New_York"));
		System.out.println(now);
	}
	
	public void calculateDate(){
		LocalDate date1=LocalDate.now(ZoneId.of("Asia/Singapore"));
		LocalDate date2=LocalDate.of(2018, 9, 1);
		int diff=date1.compareTo(date2);
		System.out.println(diff + " days to go!");
		LocalDateTime dt1=LocalDateTime.now();
		LocalDateTime dt2=LocalDateTime.of(2018, 9,10,12,2,3);
		long hourDiff=ChronoUnit.HOURS.between(dt1,dt2);
		System.out.println(hourDiff + " hours inbetween");
		
		LocalDateTime dt3=dt1.minusDays(10);
		System.out.println(  dt3 + "\t10 days behind");
		
		ZonedDateTime dt4=dt3.atZone(ZoneId.of("Asia/Singapore"));
		System.out.println( dt4 + "\tWith Timezoneof Asia/Singapore" );
		
		ZonedDateTime dt5=dt3.atZone(ZoneId.of("America/New_York"));
		System.out.println(dt5+"\tWith Timezone of America/New_York");
		
		
		DateTimeFormatter formator=DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		LocalDateTime dt6=LocalDateTime.parse("2017-08-09 05:29:00",formator);
		System.out.println(dt6.getDayOfMonth() + " is the day of month!"); 
	}
	
	public void timestamp(){
		Instant timestamp = Instant.now();
		System.out.println("Current Timestamp = "+timestamp);
		
		//Instant from timestamp
		Instant specificTime = Instant.ofEpochMilli(timestamp.toEpochMilli());
		System.out.println("Specific Time = "+specificTime);
		
		//Duration example
		Duration thirtyDay = Duration.ofDays(30);
		System.out.println(thirtyDay);
		
		System.out.println(thirtyDay.getSeconds() + " seconds for this duration!");
	}
	
	public void timeMath(){
		LocalDateTime t1=LocalDateTime.now();
		System.out.println(t1.getDayOfYear() + " day in " + t1.getYear());
		
	}

}
